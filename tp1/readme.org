* Programmer en récursif sur des tableaux

Les fonctions récursives sur les tableaux les plus simples suivent le schéma
suivant:

1. traiter le cas du tableau vide, ou réduit à un seul élément: cas terminal.
2. Pour les autres cas
  - traiter la première case
  - appeler récursivement la fonction sur le reste du tableau
  - retourner un résultat en combinant les 2 précédents.

Les tests unitaires associés vont illustrer les 2 cas, cas terminal et cas
récursif.

** Exercice basique : length

Commençons par quelque chose de basique: ~length(a)~ va renvoyer la longueur du
tableau ~a~ passé en argument.

#+begin_src javascript
const length = a => {

}
#+end_src

Commençons par spécifier le cas terminal: pour un tableau vide, la valeur à
retourner est 0:

#+begin_src javascript
const length = a => {
  if (a[0] === undefined)
    return 0
}
#+end_src

Ensuite, le cas récursif:

#+begin_src javascript
const length = a => {
  if (a[0] === undefined)
    return 0
  return 1 + length(a.slice(1))
}
#+end_src

** Exercice basique : sum
À peine plus compliqué, mais cette fois c'est à vous de jouer:
Réimplémentez la fonction
=sum=, qui retourne la somme des valeurs d'un tableau, de manière récursive.

Le squelette de la fonction à écrire est:

#+BEGIN_SRC javascript
const sum = (a) => {
}
#+END_SRC

Gérez d'abord le cas terminal, puis l'appel récursif.

** Exercice basique: map

Réimplementez la fonction =map= sous la forme =map(a,f)= qui renvoie une copie
du tableau =a=, dont les éléments ont été transformés en appelant =f=.

** Exercice intermédiaire: first

À vous de jouer: spécifiez avec des tests unitaires puis implémentez la fonction
=first(a, f)= qui renvoie le premier élément =e= de =a= pour lequel =f(e)=
renvoie =true=.

Pensez à spécifier les cas à problème:
- tableau vide
- pas d'élément trouvé vérifiant =f=

*** Variante

Écrivez à partir de =first= la fonction =some(a, f)= qui renvoie =true= si un
des élément de =a= vérifie =f=. 

** Exercice avancé: reduce

Réimplémentez la fonction =reduce= standard.

** Intermède: Fonction récursive simple ou reduce ?

La fonction =reduce= parcours systématiquement toutes les valeurs du tableau, en
les traitant les unes après les autres, en combinant éventuellement avec le
résultat de l'étape précédente.

Réimplémentez =map= à partir de =reduce=.

Écrire une application de =reduce= consiste à trouver la fonction de réduction
=(x,y) => f(x,y)=. Pour cela, suivez le raisonnement suivant:

1. Spécifier le cas terminal, soit le tableau vide =[]= soit un tableau réduit à
   un seul élément. Cela vous donne le type de résultat renvoyé par =f=, ainsi
   que le type de l'argument =x=, qui correspond au résultat accumulé par les
   applications précédentes de =f=.
2. Simuler sur un cas simple la réduction. Pour cela, écrivez sur une ligne la
   valeur initiale (soit la première du tableau, soit la valeur spécifiée en cas
   de tableau vide), puis les valeurs du tableau choisis pour votre exemple.
   Combinez ces valeurs 2 à 2, déduisez-en le code de =f=.

Exemple avec =map=:

1. =map([], f) = []=, quelle que soit =f=.
2. =map([1,2,3], (x) => x + 1)=. Les valeurs à combiner par la réduction sont :
   =[] 1 2 3=. 

La fonction de réduction est de la forme =comb(accu, e)= avec =accu= qui est un
tableau (déduit grâce au cas de base), et =e= qui est un des éléments du tableau
initial.

Avec ~f = (x) => x + 1~: 

- Combinaison 1 :: =comb([], 1)= doit retourner =[f(1)]=, soit =[2]=
- Combinaison 2 :: =comb([2], 2)= doit retourner =[2, f(2)]=

On voit rapidement que ~comb = (accu, e) => {accu.push(f(e)); return accu}~

Le résultat final est donc:

#+BEGIN_SRC javascript
const map = (a, f) => a.reduce((accu, e) => {accu.push(f(e)); return accu}, [])
#+END_SRC

Spécifiez cette fonction par des tests unitaires.

*** filter avec reduce

Spécifiez et implémenter =filter= avec =reduce=.
*** all avec reduce

Spécifiez et implémentez la fonction =all(a, f)= qui renvoie =true= si tous les
éléments =e= de =a= vérifient ~f(e) === true~. Utilisez =reduce= pour cela.

*** first/some avec reduce

Spécifiez et implémentez =first= ou =some= avec =reduce=.
Vous devriez pouvoir recycler les tests précédents.

Pourquoi cette implémentation n'est-elle pas préférable à la précédente ?

Pensez en nombre d'opérations à effectuer.

